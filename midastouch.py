#!/usr/bin/python
# http://www.youtube.com/watch?v=bkXxQL-nyQw
# I've got the Midas Touch, everything I touch turns to G0LD!

import os, audioop, binascii
from lib import png
from string import lower
from subprocess import call

imWidth = 320
imHeight = 240
imColors = 16
imDepth = 4

rawWidth = 480

def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

def detectType(original):
    # Quick and dirty
    imext = ['.jpg','.jpeg','.png']
    auext = ['.wav','.raw']
    ext = os.path.splitext(original)[1]
    if lower(ext) in imext:
        return "im"
    elif lower(ext) in auext:
        return "au"


def downsampleImage(photo):
    call('convert -adaptive-resize '+str(imWidth)+'x'+
         #str(imHeight)+'! -depth '+str(imDepth)+' -dither FloydSteinberg "orig/'+
         str(imHeight)+'! -dither FloydSteinberg "orig/'+
         photo+'" "thumb/'+os.path.splitext(photo)[0]+'-downsampled.png"',shell=True)


def rawHeader(filetype):
    header = ''
    mark = {'im':'0110100101101101','au':'0110000101110101'}
    copies = (rawWidth/len(mark[filetype]))*4
    for loop in range(copies):
        header += mark[filetype]
    return header
    

def writePNG(bitstream, filename):
    smap = chunks(bitstream, rawWidth)
    smap = map(lambda x: map(int, x), smap)
    rawimfile = open('png/'+filename+'-raw.png', 'wb')
    rawim = png.Writer(len(smap[0]), len(smap), greyscale=True, bitdepth=1)
    rawim.write(rawimfile, smap)
    rawimfile.close()


def makeRawImage(photo):
    bitstream = rawHeader('im')
    filename = os.path.splitext(photo)[0]
    src = png.Reader('thumb/'+filename+'-downsampled.png')
    w, h, pixeldata, metadata = src.asRGB8()
    for row in pixeldata:
        for pixel in row:
            print pixel/16
            #for binary in str(bin(pixel/(256/imColors)))[2:].zfill(imDepth):
            for binary in str(bin(pixel/16))[2:].zfill(imDepth):
                bitstream+=binary
    writePNG(bitstream, filename)

def makeRawAudio(audio):
    bitstream = rawHeader('au')
    filename = os.path.splitext(audio)[0]
    src = open('orig/'+audio, 'rb')
    raw = src.read()
    for byte in raw:
        bitstream+='{0:08b}'.format(ord(byte))
    complete = len(bitstream)/rawWidth
    incomplete = len(bitstream)-(complete*rawWidth)
    padding = rawWidth-incomplete
    bitstream+='0'*padding 
    writePNG(bitstream, filename)


originals = os.listdir('orig')
originals.sort()

for original in originals:
    filetype = detectType(original)
    if filetype == "im":
        print '[*] processing '+original+' (image)'
        print '[.] downsampling image'
        downsampleImage(original)
        print '[.] raw conversion'
        makeRawImage(original)
    elif filetype == "au":
        print '[*] processing '+original+' (audio)'
        print '[.] raw conversion'
        makeRawAudio(original)
