#!/usr/bin/python
# Sandbox decoder
# Image input must be 1-bit grayscale (yes)

import os
from lib import png

rawWidth = 480
rawHeight = 1920

imWidth = 320
imHeight = 240
imColors = 16
imDepth = 4

bitstream = []

def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]

r=png.Reader('test.png')
w, h, pixeldata, metadata = r.asDirect()
for row in pixeldata:
    for pixel in row:
        if pixel == 0:
            bitstream.append('0')
        else:
            bitstream.append('1')

print len(bitstream)

pixels = chunks(bitstream,4)
bRGB = chunks(pixels,3)
RGB = []
for bits in bRGB:
    bR, bG, bB = bits
    R = int(''.join(bR),2)*imColors
    RGB.append(R)
    G = int(''.join(bG),2)*imColors
    RGB.append(G)
    B = int(''.join(bB),2)*imColors
    RGB.append(B)


p = chunks(RGB,imWidth*3)

f = open('decoded.png', 'wb')
w = png.Writer(imWidth, imHeight)
w.write(f, p) ; f.close()

#r=png.Reader('test.png')
#w, h, pixeldata, metadata = r.asDirect()
#for row in pixeldata:
#    for pixel in row:
#        if pixel == 0:
#            bitstream.append('0')
#        else:
#            bitstream.append('1')
#
#bytes = chunks(bitstream,8)
#
#f = open("decoded.raw", "wb")
#
#for byte in bytes:
#    char = chr(int(''.join(byte), 2))
#    f.write(char)
#f.close()



